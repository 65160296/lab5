/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.util.Scanner;

/**
 *
 * @author Tncpop
 */
public class Game {
    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }
    public void play(){
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                saveStat();
                showWin();
                break;
            }
            if(table.checkDraw()){
                saveDraw();
                showDraw();
                break;
            }
            table.switchPlayer();
        }
    }
    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }
      private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
       private void newGame() {
        table = new Table(player1,player2);
    }
        private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.err.println("Please input row col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void saveStat() {
       if(table.getCurrentPlayer()==player1) {
           player1.win();
           player2.lose();
           
       }else{
           player1.lose();
           player2.win();
       }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
        
    }

    private void showWin() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!");
    }

    private void showDraw() {
        System.out.println("It's Draw");
    }





}
